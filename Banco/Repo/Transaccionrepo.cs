﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using WebApplication3.DB;
using WebApplication3.Models;

namespace WebApplication3.Repo
{
    public interface ITransaccionrepo
    {
        List<Transaccion> GetTransaccions(int IdCuenta);
        Cuenta GetCuenta(int IdCuenta);
        void SaveTransaccion(Transaccion tr);
        void ModificarCuenta(Cuenta cuenta);
    }

    public class Transaccionrepo : ITransaccionrepo
    {
        private readonly AppPruebaContext context;

        public Transaccionrepo(AppPruebaContext context)
        {
            this.context = context;
        }
        public Cuenta GetCuenta(int IdCuenta)
        {
            return context.Cuentas
                .Where(o => o.IdCuenta == IdCuenta)
                .FirstOrDefault();
        }

        public List<Transaccion> GetTransaccions(int IdCuenta)
        {
            return context.Transaccions
                .Include(o => o.Cuenta)
                .Where(o => o.IdCuenta == IdCuenta)
                .ToList();
        }

        public void ModificarCuenta(Cuenta cuenta)
        {
            context.Entry(cuenta).State = (Microsoft.EntityFrameworkCore.EntityState)EntityState.Modified;
            context.SaveChanges();
        }

        public void SaveTransaccion(Transaccion tr)
        {
            context.Transaccions.Add(tr);
            context.SaveChanges();
        }
    }
}
