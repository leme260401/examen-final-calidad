﻿using System.Collections.Generic;
using System.Linq;
using WebApplication3.DB;
using WebApplication3.Models;

namespace WebApplication3.Repo
{
    public interface ICuentarepo
    {
        List<Cuenta> GetCuentas();
        void SaveCuenta(Cuenta cuenta);
    }

    public class Cuentarepo : ICuentarepo
    {
        private readonly AppPruebaContext context;

        public Cuentarepo(AppPruebaContext context)
        {
            this.context = context;
        }

        public List<Cuenta> GetCuentas()
        {
            return context.Cuentas.ToList();
        }

        public void SaveCuenta(Cuenta cuenta)
        {
            context.Cuentas.Add(cuenta);
            context.SaveChanges();
        }
    }
}
