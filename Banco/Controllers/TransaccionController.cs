﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication3.DB;
using WebApplication3.Models;
using WebApplication3.Repo;

namespace WebApplication3.Controllers
{
    public class TransaccionController : Controller
    {
        private ITransaccionrepo context;
        public TransaccionController(ITransaccionrepo context)
        {
            this.context = context;
        }

        public IActionResult Index(int IdCuenta)
        {
            var transacciones = context.GetTransaccions(IdCuenta);
            return View("Index", transacciones);
        }
        [HttpGet]
        public IActionResult Create(int IdCuenta)
        {
            ViewBag.Cuenta = context.GetCuenta(IdCuenta);

            return View("Create", new Transaccion());
        }
        [HttpPost]
        public IActionResult Create(Transaccion transaccion)
        {
            var a = verificarCambios(transaccion);
            if (a == false)
                ModelState.AddModelError("Ingreso", "No se puede gastar lo que no se tiene");

            if (!ModelState.IsValid)
            {
                ViewBag.Cuenta = context.GetCuenta(transaccion.IdCuenta);

                return View("Create", transaccion);
            }
            transaccion.Fecha = DateTime.Now.ToString();
            context.SaveTransaccion(transaccion);
            
            return RedirectToAction("Index", new { transaccion.IdCuenta});
        }

        private bool verificarCambios(Transaccion transaccion)
        {
            var cuenta = context.GetCuenta(transaccion.IdCuenta);
            bool a = true;

            if (transaccion.Tipo == true) {
                cuenta.SaldoInicial += transaccion.Monto;
            }
            else if (transaccion.Tipo == false)
            {
                if (cuenta.LimiteCuenta < transaccion.Monto)
                {
                    a = false;
                    return a;
                }
                else
                {
                    cuenta.SaldoInicial -= transaccion.Monto;
                }
            }
            
            //context.Entry(cuenta).State = EntityState.Modified;
            //context.SaveChanges();
            return a;
        }
    }
}
