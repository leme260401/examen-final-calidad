﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.DB;
using WebApplication3.Models;
using WebApplication3.Repo;

namespace WebApplication3.Controllers
{
    public class CuentaController : Controller
    {
        private ICuentarepo context;
        public CuentaController(ICuentarepo context)
        {
            this.context = context;
        }
        public IActionResult Index()
        {
            var cuenta = context.GetCuentas();

            return View("Index", cuenta);
        }
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Categoria = GetCategorias();
            return View("Create", new Cuenta());
        }
        [HttpPost]
        public IActionResult Create(Cuenta cuenta)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Categoria = GetCategorias();
                return View("Create", cuenta);
            }

            cuenta.LimiteCuenta = cuenta.SaldoInicial;

            context.SaveCuenta(cuenta);
            return RedirectToAction("Index");
        }
        private List<String> GetCategorias()
        {
            return new List<string>
            {
                "Propia","Credito"
            };
        }
    }
}
