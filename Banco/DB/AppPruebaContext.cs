﻿using Microsoft.EntityFrameworkCore;
using WebApplication3.Controllers;
using WebApplication3.DB.Configurations;
using WebApplication3.Models;

namespace WebApplication3.DB
{
    public class AppPruebaContext : DbContext
    {
        public AppPruebaContext(DbContextOptions<AppPruebaContext> o) : base(o) { }

        public DbSet<Cuenta> Cuentas { get; set; }
        public DbSet<Transaccion> Transaccions { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new CuentaConfigurations());
            modelBuilder.ApplyConfiguration(new TransaccionConfigurations());
        }
    }
}
