﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication3.Controllers;
using WebApplication3.Models;
using WebApplication3.Repo;

namespace WebAppTest
{
    [TestFixture]
    internal class TransaccionTest
    {
        [Test]
        public void IndexTest()
        {
            var repo = new Mock<ITransaccionrepo>();
            repo.Setup(o => o.GetTransaccions(5)).Returns(new List<Transaccion>());
            var controller = new TransaccionController(repo.Object);
            var view = controller.Index(5) as ViewResult;

            Assert.AreEqual("Index", view.ViewName);
        }
        [Test]
        public void CreateTest()
        {
            var repo = new Mock<ITransaccionrepo>();
            repo.Setup(o => o.GetTransaccions(5)).Returns(new List<Transaccion>());
            var controller = new TransaccionController(repo.Object);
            var view = controller.Create(5) as ViewResult;

            Assert.AreEqual("Create", view.ViewName);
        }
        [Test]
        public void CreateTestPostError()
        {
            var repo = new Mock<ITransaccionrepo>();
            repo.Setup(o => o.GetCuenta(5)).Returns(new Cuenta() { IdCuenta = 5 , SaldoInicial = 100, LimiteCuenta = 0});
            var controller = new TransaccionController(repo.Object);
            var view = controller.Create(new Transaccion() { IdCuenta=5, Monto = 10}) as ViewResult;

            Assert.AreEqual("Create", view.ViewName);
        }

        [Test]
        public void CreateTestPostCorrecto()
        {
            var repo = new Mock<ITransaccionrepo>();
            repo.Setup(o => o.GetCuenta(5)).Returns(new Cuenta() { IdCuenta = 5, SaldoInicial = 100, LimiteCuenta = 100 });
            var controller = new TransaccionController(repo.Object);
            var view = controller.Create(new Transaccion() { IdCuenta = 5, Monto = 10 }) as RedirectToActionResult;

            Assert.AreEqual("Index", view.ActionName);
        }
    }
}
