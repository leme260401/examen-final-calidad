﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication3.Controllers;
using WebApplication3.Models;
using WebApplication3.Repo;

namespace WebAppTest
{
    [TestFixture]
    internal class CuentaTest
    {
        [Test]
        public void IndexTest()
        {
            var repo = new Mock<ICuentarepo>();

            var controller = new CuentaController(repo.Object);
            var view = controller.Index() as ViewResult;

            Assert.AreEqual("Index", view.ViewName);
        }

        [Test]
        public void CreateTest()
        {
            var repo = new Mock<ICuentarepo>();

            var controller = new CuentaController(repo.Object);
            var view = controller.Create() as ViewResult;

            Assert.AreEqual("Create", view.ViewName);
        }

        [Test]
        public void CreateTestPost()
        {
            var repo = new Mock<ICuentarepo>();
            repo.Setup(o => o.SaveCuenta(new Cuenta()));

            var controller = new CuentaController(repo.Object);
            var view = controller.Create() as ViewResult;

            Assert.AreEqual("Create", view.ViewName);
        }
    }
}
